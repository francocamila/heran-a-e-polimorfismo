#include <math.h>
#include <iostream>
#include "hexagono.hpp"

using namespace std;

Hexagono::Hexagono(float base, float altura){
    set_tipo("Hexagono");
    set_base(base);
    set_altura(altura);

}

float Hexagono::calcula_area(){
    return 6*(get_altura()*get_base()/2);
}

float Hexagono::calcula_perimetro(){
    return 6*get_base();
}