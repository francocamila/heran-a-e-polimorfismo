#include <math.h>
#include <iostream>
#include "paralelogramo.hpp"

using namespace std;


Paralelogramo::Paralelogramo(float base, float altura){
    set_tipo("Paralelogramo");
    set_base(base);
    set_altura(altura);

}

float Paralelogramo::calcula_area(){
    return get_base()*get_altura();
}

float Paralelogramo::calcula_perimetro(){
    return 4*get_base();
}