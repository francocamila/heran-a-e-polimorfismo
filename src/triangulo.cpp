#include <math.h>
#include <iostream>
#include "triangulo.hpp"

using namespace std;

Triangulo::Triangulo(float base, float altura){
    set_tipo("Triangulo");
    set_base(base);
    set_altura(altura);

}

float Triangulo::calcula_area(){
    return get_base()*get_altura()/2.0;
}

float Triangulo::calcula_perimetro(){
    float l;
    l = sqrt(pow((get_base()/2.0),2)+pow(get_altura()/2.0,2));
    return 2.0*l+get_base();
}