#include <math.h>
#include <iostream>
#include "circulo.hpp"

using namespace std;

Circulo::Circulo(float base, float altura){
    set_tipo("Circulo");
    set_base(base);
    set_altura(altura);

}

float Circulo::calcula_area(){
    float pi = 3.14;
    return pi*(pow(get_altura(),2));
}

float Circulo::calcula_perimetro(){
    float pi = 3.14;
    return 2*pi*get_altura();
}