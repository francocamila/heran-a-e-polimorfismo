#include <math.h>
#include <iostream>
#include "quadrado.hpp"

using namespace std;

Quadrado::Quadrado(float base, float altura){
    set_tipo("Quadrado");
    set_base(base);
    set_altura(altura);

}

float Quadrado::calcula_area(){
    return get_base()*get_altura();
}

float Quadrado::calcula_perimetro(){
    return 2*get_base()+2*get_altura();
}