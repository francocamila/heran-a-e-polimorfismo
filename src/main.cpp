#include <math.h>
#include <iostream>
#include <string.h>
#include "formageometrica.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

using namespace std;

int main(int argc, char ** argv){
    FormaGeometrica * forma1 = new FormaGeometrica();
    Triangulo * forma2 = new Triangulo(1.0, 2.0);
    Quadrado * forma3 = new Quadrado(1.0, 2.0);
    Circulo * forma4 = new Circulo(1.0, 1.0);
    Paralelogramo * forma5 = new Paralelogramo(1.0, 2.0);
    Pentagono * forma6 = new Pentagono(1.0, 2.0);
    Hexagono * forma7 = new Hexagono(1.0, 2.0);
    FormaGeometrica * forma8 = new FormaGeometrica("Triangulo",3.0,4.0);
    FormaGeometrica * forma9 = new FormaGeometrica("Quadrado",3.0,4.0);
    FormaGeometrica * forma10 = new FormaGeometrica("Circulo",3.0,4.0);
    FormaGeometrica * forma11 = new FormaGeometrica("Paralelogramo",3.0,4.0);
    FormaGeometrica * forma12 = new FormaGeometrica("Pentagono",3.0,4.0);
    FormaGeometrica * forma13 = new FormaGeometrica("Hexagono",3.0,4.0);

    FormaGeometrica * lista[20];

    lista[0] = forma1;
    lista[1] = forma2;
    lista[2] = forma3;
    lista[3] = forma4;
    lista[4] = forma5;
    lista[5] = forma6;
    lista[6] = forma7;
    lista[7] = forma8;
    lista[8] = forma9;
    lista[9] = forma10;
    lista[10] = forma11;
    lista[11] = forma12;
    lista[12] = forma13;

    for(int i =0; i<13; i++){
        cout << lista[i]->get_tipo() << ":" << "Área:" << lista[i]->calcula_area() << "," << "Perímetro:" << lista[i]->calcula_perimetro() << endl;
    }
}

