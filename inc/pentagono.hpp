#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include <string>

#include "formageometrica.hpp"

class Pentagono: public FormaGeometrica {
    public:
        
        Pentagono(float base, float altura);
        
        float calcula_area();
        float calcula_perimetro();
};

#endif