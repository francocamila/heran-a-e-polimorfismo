#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include <string>

#include "formageometrica.hpp"

class Circulo: public FormaGeometrica {
    public:
        
        Circulo(float base, float altura);
        
        float calcula_area();
        float calcula_perimetro();
};

#endif