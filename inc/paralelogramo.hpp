#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include <string>

#include "formageometrica.hpp"

class Paralelogramo: public FormaGeometrica {
    public:
        
        Paralelogramo(float base, float altura);
        
        float calcula_area();
        float calcula_perimetro();
};

#endif