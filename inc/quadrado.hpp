#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include <string>

#include "formageometrica.hpp"

class Quadrado: public FormaGeometrica {
    public:
        
        Quadrado(float base, float altura);
        
        float calcula_area();
        float calcula_perimetro();
};

#endif