#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include <string>

#include "formageometrica.hpp"

class Hexagono: public FormaGeometrica {
    public:
        
        Hexagono(float base, float altura);
        
        float calcula_area();
        float calcula_perimetro();
};

#endif